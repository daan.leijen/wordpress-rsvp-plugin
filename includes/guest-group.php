<?php
if (!defined('ABSPATH')) {
    die('You cannot be here');
}

use Carbon_Fields\Field;
use Carbon_Fields\Container;

ob_start();

add_action('init', 'register_guest_group_custom_post_type');

/**
 * Register a Guest Group post type. The post type is named "Guest Group".
 *
 * @return void.
 */
function register_guest_group_custom_post_type()
{

    $labels = apply_filters('w_rsvp_guest_group_post_type_labels', [
        'name' => 'Guest Groups',
        'singular_name' => 'Guest Group',
        'plural_name' => 'Guest Groups',
        'add_new' => 'Add new guest group',
        'add_new_item' => 'Add new guest group',
        'edit_item' => 'Edit guest group',
        'new_item' => 'New guest group',
        'all_items' => 'All guest groups',
        'search_items' => 'Search guest group',
        'not_found' => 'No guest group found',
        'not_found_in_trash' => 'No guest group found in trash',
        'parent_item_colon' => '’',
        'menu_name' => 'Guest Groups',
    ]);

    // Custom post type arguments, which can be filtered if needed.
    $args = apply_filters(
        'w_rsvp_guest_group_post_type_args',
        [
            'labels' => $labels,
            'description' => 'Displays the overview of the wedding guest groups',
            'public' => true,
            'publicly_queryable' => false,
            'show_ui' => true,
            'menu_position' => 303,
            'menu_icon' => 'dashicons-groups',
            'supports' => false,
            'show_in_rest' => true,
        ]
    );

    register_post_type('w_rsvp_guest_group', $args);
}

add_filter('wp_insert_post_data', 'filter_post_data_guest_group', 99, 2);
function filter_post_data_guest_group($postData, $postarr)
{

    if ($postData['post_type'] == 'w_rsvp_guest_group' && $postData['post_status'] == 'draft') {
        $postData['post_status'] = 'publish';
    }

    return $postData;
}

add_action('acf/save_post', 'save_post_handler', 20);
function save_post_handler($post_id)
{
    if (get_post_type($post_id) == 'w_rsvp_guest_group') {
        $guests_arr = carbon_get_post_meta($post_id, 'guest_group_guests');
        $title = determine_joined_guest_names(array_map(function ($guest) {
            return $guest['name'];
        }, $guests_arr));
        $data['post_title'] = $title;
        $data['post_name'] = sanitize_title($title);
        wp_update_post($data);
    }
}

add_action('carbon_fields_register_fields', 'register_fields_guest_group');
function register_fields_guest_group()
{
    $entry_moments = explode(',', get_plugin_options('w_rsvp_entry_moments'));

    $retrieve_or_generate_passcode_func = function () {
        global $pagenow;
        if (in_array($pagenow, array('post-new.php'))) {
            // Adding a new guest group, so generate a new passcode.
            return generate_unique_passcode();
        } else {
            $curr_passcode = carbon_get_the_post_meta('guest_group_passcode');
            return $curr_passcode;
        }
    };

    $guests_labels = array(
        'plural_name' => 'Guests',
        'singular_name' => 'Guest',
    );

    Container::make('post_meta', 'Guest Group')
        ->where('post_type', '=', 'w_rsvp_guest_group') // only show our new fields on pages
        ->add_fields(array(
            Field::make('complex', 'guest_group_guests', 'Guests')
                ->setup_labels($guests_labels)
                ->set_min(1)
                ->set_layout('grid')
                ->add_fields(array(
                    Field::make('text', 'name', 'Name'),
                ))
                ->set_header_template('
                    <% if (name) { %>
                        <%- name %>
                    <% } %>
                '),
            Field::make('radio', 'guest_group_entry_moment', 'Entry moment')
                ->add_options($entry_moments),
            Field::make('text', 'guest_group_passcode', 'Passcode')
                ->set_default_value($retrieve_or_generate_passcode_func())
                ->set_attribute('readOnly', 'true'),
            Field::make('radio', 'guest_group_has_responded', 'Has responded')
                ->set_default_value(false)
                ->add_options([false => 'No', true => 'Yes']),
        ));
}

function generate_unique_passcode()
{
    $generated_passcode = '';
    $query = null;

    do {
        $generated_passcode = generate_passcode();
        $query = new WP_Query(array(
            'post_type' => 'w_rsvp_guest_group',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'meta_query' => array(
                array(
                    'key' => 'guest_group_passcode',
                    'value' => $generated_passcode,
                ),
            ),
        ));
    } while ($query->have_posts());

    return $generated_passcode;
}

add_filter('manage_w_rsvp_guest_group_posts_columns', 'create_columns_guest_group');
function create_columns_guest_group($columns)
{
    // Edit the columns for the guest group table
    $columns = array(

        'cb' => $columns['cb'],
        'guests' => 'Guest(s)',
        'entry-moment' => 'Entry moment',
        'passcode' => 'Passcode',
        'has_responded' => 'Has responded',
    );

    return $columns;
}

add_action('manage_w_rsvp_guest_group_posts_custom_column', 'fill_columns_guest_group', 10, 2);
function fill_columns_guest_group($column, $post_id)
{
    // Return meta data for individual posts on table

    $map_guest_name_func = function (array $guest): string {
        return $guest['name'];
    };

    switch ($column) {

        case 'guests':
            $guests_arr = carbon_get_post_meta($post_id, 'guest_group_guests');
            echo determine_joined_guest_names(array_map($map_guest_name_func, $guests_arr));
            break;

        case 'entry-moment':
            $entry_moment_key = carbon_get_post_meta($post_id, 'guest_group_entry_moment');
            echo explode(',', get_plugin_options('w_rsvp_entry_moments'))[$entry_moment_key];
            break;

        case 'passcode':
            echo carbon_get_post_meta($post_id, 'guest_group_passcode');
            break;

        case 'has_responded':
            echo carbon_get_post_meta($post_id, 'guest_group_has_responded') ? 'Yes' : 'No';
            break;
    }
}

add_action('rest_api_init', function () {
    register_rest_route('wedding-RSVP/v1', '/guest-group', array(
        'methods' => 'GET',
        'callback' => 'retrieve_guest_group_via_passcode',
        'permission_callback' => '__return_true',
        'args' => array(
            'passcode' => array(
                'required' => true,
            ),
        ),
    ));
});

function retrieve_guest_group_via_passcode($data)
{
    $map_guest_name_func = function (array $guest): string {
        return $guest['name'];
    };

    $query = new WP_Query(array(
        'post_type' => 'w_rsvp_guest_group',
        'post_status' => 'publish',
        'meta_query' => array(
            array(
                'key' => 'guest_group_passcode',
                'value' => $data['passcode'],
            ),
        ),
    ));

    if (!$query->have_posts()) {
        return new WP_Error('no_guest_group', 'Passcode invalid', array('status' => 404));
    }

    $guest_group_id = $query->get_posts()[0]->ID;
    $guests_arr = carbon_get_post_meta($guest_group_id, 'guest_group_guests');
    $guests_name_arr = array_map($map_guest_name_func, $guests_arr);
    $guests_joined_names = determine_joined_guest_names($guests_name_arr);

    $response_body = json_encode([
        'id' => $guest_group_id,
        'guests_joined' => $guests_joined_names,
        'guests' => array_map($map_guest_name_func, $guests_arr),
        'entry-moment' => explode(',', get_plugin_options('w_rsvp_entry_moments'))[carbon_get_post_meta($guest_group_id, 'guest_group_entry_moment')],
        'entry-moment-raw' => carbon_get_post_meta($guest_group_id, 'guest_group_entry_moment'),
        'has_responded' => carbon_get_post_meta($guest_group_id, 'guest_group_has_responded')
    ]);

    return new WP_REST_Response(
        array(
            'status' => 200,
            'body_response' => $response_body
        )
    );
}


add_action('admin_menu', 'guest_groups_add_export_menu_item');
function guest_groups_add_export_menu_item()
{
    add_submenu_page('edit.php?post_type=w_rsvp_guest_group', 'Export', 'Export', 'manage_options', 'export_guest_groups', 'export_guest_groups');
}

function export_guest_groups()
{
    if (is_admin()) {
        ob_clean();

        $fh = @fopen('php://output', 'w');
        $header_row = array(
            0 => 'Guest(s)',
            1 => 'Entry moment',
            2 => 'Passcode',
            3 => 'Has responded'
        );

        fputcsv($fh, $header_row);
        fprintf($fh, chr(0xEF) . chr(0xBB) . chr(0xBF));

        $query = new WP_Query(array(
            'post_type' => 'w_rsvp_guest_group',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'order' => 'ASC',
            'orderby' => 'ID',
        ));

        foreach ($query->get_posts() as $guest_group) {
            $guest_group_id = $guest_group->ID;
            $guests_arr = carbon_get_post_meta($guest_group_id, 'guest_group_guests');
            $guests_name_arr = array_map(function (array $guest): string {
                return $guest['name'];
            }, $guests_arr);
            $guests_joined_names = determine_joined_guest_names($guests_name_arr);

            fputcsv($fh, [
                0 => $guests_joined_names,
                1 => explode(',', get_plugin_options('w_rsvp_entry_moments'))[carbon_get_post_meta($guest_group_id, 'guest_group_entry_moment')],
                2 => carbon_get_post_meta($guest_group_id, 'guest_group_passcode'),
                3 => carbon_get_post_meta($guest_group_id, 'guest_group_has_responded')
            ]);
        }
        fclose($fh);

        header("Content-Type: text/csv");
        header("Content-Disposition: attachment; filename=guests.csv");
        exit();
    }
}

/**
 * @param array $guests_name_arr
 * @return string The joined guest names.
 */
function determine_joined_guest_names(array $guests_name_arr): string
{
    $last = array_pop($guests_name_arr);
    if (!empty($guests_name_arr)) {
        $guests_joined_names = join(", ", $guests_name_arr) . ' & ' . $last;
    } else {
        $guests_joined_names = $last;
    }
    return $guests_joined_names;
}