<?php

if( !defined('ABSPATH') )
{
      die('You cannot be here');
}

function get_plugin_options($name)
{
      return carbon_get_theme_option( $name );
}

function generate_passcode()
{
    $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $digits = '0123456789';
    $specials = '!@#$%^&?';

    $passcode_format = get_plugin_options('w_rsvp_passcode_format');
    $passcode = '';

    foreach (str_split($passcode_format) as $char) {
        switch ($char) {
            case '*':
                $passcode .= $characters[random_int(0, strlen($characters) - 1)];
                break;
            case '#':
                $passcode .= $digits[random_int(0, strlen($digits) - 1)];
                break;
            case '!':
                $passcode .= $specials[random_int(0, strlen($specials) - 1)];
                break;
        }
    }

    return $passcode;
}