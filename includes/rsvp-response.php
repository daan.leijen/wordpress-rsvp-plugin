<?php
if (!defined('ABSPATH')) {
    die('You cannot be here');
}

ob_start();

add_action('init', 'register_rsvp_response_custom_post_type');
function register_rsvp_response_custom_post_type()
{
    $args = [
        'public' => true,
        'has_archive' => true,
        'menu_position' => 303,
        'publicly_queryable' => false,
        'labels' => [

            'name' => 'RSVP Responses',
            'singular_name' => 'RSVP Response',
            'edit_item' => 'View RSVP Response'

        ],
        'supports' => false,
        'capability_type' => 'post',
        'capabilities' => array(
            'create_posts' => 'do_not_allow',
        ),
        'map_meta_cap' => false
    ];

    register_post_type('w_rsvp_response', $args);
}

add_filter('wp_insert_post_data', 'filter_post_data_rsvp_response', 99, 2);
function filter_post_data_rsvp_response($postData, $postarr)
{

    if ($postData['post_type'] == 'w_rsvp_response' && $postData['post_status'] == 'draft') {
        $postData['post_status'] = 'publish';
    }

    return $postData;
}

add_action('rest_api_init', 'create_rest_endpoint');
function create_rest_endpoint()
{
    // Create endpoint for front end to connect to WordPress securely to post form data
    register_rest_route('wedding-RSVP/v1', 'rsvp-response/submit', array(
        'methods' => 'POST',
        'callback' => 'handle_submission',
        'permission_callback' => '__return_true',
    ));
}

function handle_submission($data)
{
    // Get all parameters from form
    $params = $data->get_params();
    $form = json_decode($params['form'], true);

    foreach ($form['guest_answers'] as $guest_name => $guest_answers) {
        $postarr = [
            'post_title' => $guest_name,
            'post_type' => 'w_rsvp_response',
            'post_status' => 'publish'
        ];

        $post_id = wp_insert_post($postarr);

        // Loop through each field posted and sanitize it
        foreach ($guest_answers['choice_answers'] as $key => $value) {
            add_post_meta($post_id, $key, $value);
        }
        foreach ($guest_answers['comment_answers'] as $key => $value) {
            sanitize_textarea_field(add_post_meta($post_id, $key, $value));
        }
    }

    carbon_set_post_meta($form['guests']['id'], 'guest_group_has_responded', true);

    $confirmation_message = get_plugin_options('w_rsvp_submit_message');

    return new WP_Rest_Response($confirmation_message, 200);
}

add_action('admin_menu', 'rsvp_responses_add_export_menu_item');
function rsvp_responses_add_export_menu_item()
{
    add_submenu_page('edit.php?post_type=w_rsvp_response', 'Export', 'Export', 'manage_options', 'export_RSVP_responses', 'export_RSVP_responses');
}

function export_RSVP_responses()
{
    if (is_admin()) {
        ob_clean();

        $fh = @fopen('php://output', 'w');

        $header_row = array(
            'Guest',
        );

        $query = new WP_Query(array(
            'post_type' => 'w_rsvp_question',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'order' => 'ASC',
            'orderby' => 'ID',
        ));

        foreach ($query->get_posts() as $question) {
            $header_row[] = carbon_get_post_meta($question->ID, 'question_key');
        }

        fputcsv($fh, $header_row);
        fprintf($fh, chr(0xEF) . chr(0xBB) . chr(0xBF));

        $query = new WP_Query(array(
            'post_type' => 'w_rsvp_response',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'order' => 'ASC',
            'orderby' => 'ID',
        ));

        foreach ($query->get_posts() as $response) {
            $response_id = $response->ID;
            $response_meta_fields = get_post_meta($response_id);

            $data_row = array_map(function($label) use ($response, $response_meta_fields) {
                if ($label == 'Guest') {
                    return $response->post_title;
                } else {
                    $question_response = $response_meta_fields[$label] ?? null;
                    if ($question_response != null) {
                        return $question_response[0];
                    } else {
                        return "";
                    }
                }
            }, $header_row);

            fputcsv($fh, $data_row);
        }
        fclose($fh);

        header("Content-Type: text/csv");
        header("Content-Disposition: attachment; filename=rsvp-responses.csv");
        exit();
    }
}