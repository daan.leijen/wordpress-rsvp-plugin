<?php
if (!defined('ABSPATH')) {
    die('You cannot be here');
}

use Carbon_Fields\Field;
use Carbon_Fields\Container;

const TYPE_OPTIONS = ['radio' => 'Selection', 'textarea' => 'Comment'];

add_action('init', 'register_question_custom_post_type');

/**
 * Register a Question post type. The post type is named "Question".
 *
 * @return void.
 */
function register_question_custom_post_type()
{

    $labels = apply_filters('w_rsvp_guest_group_post_type_labels', [
        'name' => 'Questions',
        'singular_name' => 'Question',
        'plural_name' => 'Questions',
        'add_new' => 'Add new question',
        'add_new_item' => 'Add new question',
        'edit_item' => 'Edit question',
        'new_item' => 'New question',
        'all_items' => 'All questions',
        'search_items' => 'Search question',
        'not_found' => 'No question found',
        'not_found_in_trash' => 'No question found in trash',
        'parent_item_colon' => '’',
        'menu_name' => 'Questions',
    ]);

    // Custom post type arguments, which can be filtered if needed.
    $args = apply_filters(
        'w_rsvp_guest_group_post_type_args',
        [
            'labels' => $labels,
            'description' => 'Displays the overview of the RSVP questions',
            'public' => true,
            'publicly_queryable' => false,
            'show_ui' => true,
            'menu_position' => 302,
            'menu_icon' => 'dashicons-clipboard',
            'supports' => false,
            'show_in_rest' => true,
        ]
    );

    register_post_type('w_rsvp_question', $args);
}

add_filter('wp_insert_post_data', 'filter_post_data_question', 99, 2);
function filter_post_data_question($postData, $postarr)
{

    if ($postData['post_type'] == 'w_rsvp_question' && $postData['post_status'] == 'draft') {
        $postData['post_status'] = 'publish';
    }

    return $postData;
}

add_action('carbon_fields_register_fields', 'register_fields_question');
function register_fields_question()
{
    $entry_moments = explode(',', get_plugin_options('w_rsvp_entry_moments'));

    $label_labels = array(
        'plural_name' => 'question labels',
        'singular_name' => 'question label',
    );

    $possible_values_labels = array(
        'plural_name' => 'values',
        'singular_name' => 'value',
    );

    $possible_value_labels_labels = array(
        'plural_name' => 'value labels',
        'singular_name' => 'value label',
    );

    $hide_for_question_value_labels = array(
        'plural_name' => 'question values to hide question for',
        'singular_name' => 'question value to hide question for',
    );

    Container::make('post_meta', 'Question')
        ->where('post_type', '=', 'w_rsvp_question') // only show our new fields on pages
        ->add_fields(array(
            Field::make('text', 'question_key', 'Key'),
            Field::make('text', 'question_position', 'Position'),
            Field::make('radio', 'question_show_per_guest', 'Ask for each guest')
                ->set_default_value(false)
                ->add_options([false => 'No', true => 'Yes']),
            Field::make('set', 'question_show_for_entry_moments', 'Show for entry moment(s)')
                ->add_options($entry_moments),
            Field::make('complex', 'question_labels', 'Label(s) for question')
                ->setup_labels($label_labels)
                ->set_min(1)
                ->set_layout('tabbed-horizontal')
                ->add_fields(array(
                    Field::make('textarea', 'label', 'Label'),
                ))
                ->set_header_template('
                    <% if (label) { %>
                        <%- label %>
                    <% } %>
                '),
            Field::make('radio', 'question_type', 'Type')
                ->add_options(TYPE_OPTIONS)
                ->set_help_text('Selection means one of a number of options must be chosen. Comments will provide a text are where text can be entered.'),
            Field::make('complex', 'possible_values', 'Possible values')
                ->setup_labels($possible_values_labels)
                ->set_conditional_logic(array(
                    array(
                        'field' => 'question_type',
                        'value' => 'radio'
                    )
                ))
                ->set_layout('tabbed-horizontal')
                ->add_fields(array(
                    Field::make('text', 'key', 'Key'),
                    Field::make('complex', 'labels', 'Label(s)')
                        ->setup_labels($possible_value_labels_labels)
                        ->set_layout('tabbed-horizontal')
                        ->add_fields(array(
                            Field::make('text', 'label', 'Label'),
                        ))
                        ->set_header_template('
                            <% if (label) { %>
                                <%- label %>
                            <% } %>
                        '),
                ))
                ->set_header_template('
                    <% if (key) { %>
                        <%- key %>
                    <% } %>
                '),
            Field::make('complex', 'question_hide_for_question_value', 'Hide question in case of other question value')
                ->setup_labels($hide_for_question_value_labels)
                ->set_layout('grid')
                ->add_fields(array(
                    Field::make('text', 'key', 'Question key'),
                    Field::make('text', 'value_key', 'Value key'),
                ))
                ->set_header_template('
                            <% if (key && value_key) { %>
                                <%- key %> - <%- value_key %>
                            <% } else { %>
                                <% if (key) { %>
                                    <%- key %>
                                <% } %>
                            <% } %>
                        '),
        ));
}

add_filter('manage_w_rsvp_question_posts_columns', 'create_columns_question');
function create_columns_question($columns)
{
    // Edit the columns for the guest group table
    $columns = array(

        'cb' => $columns['cb'],
        'key' => 'Key',
        'position' => 'Position',
        'for_each_guest' => 'Shown for each guest',
        'entry-moments' => 'Shown for entry moment(s)',
        'type' => 'Type',
    );

    return $columns;
}

add_action('manage_w_rsvp_question_posts_custom_column', 'fill_columns_question', 10, 2);
function fill_columns_question($column, $post_id)
{
    switch ($column) {

        case 'key':
            echo carbon_get_post_meta($post_id, 'question_key');
            break;

        case 'position':
            echo carbon_get_post_meta($post_id, 'question_position');
            break;

        case 'for_each_guest':
            echo carbon_get_post_meta($post_id, 'question_show_per_guest') ? 'Yes' : 'No';
            break;

        case 'entry-moments':
            $entry_moment_keys = carbon_get_post_meta($post_id, 'question_show_for_entry_moments');
            $entry_moments = explode(',', get_plugin_options('w_rsvp_entry_moments'));
            echo join(", ", array_map(
                function ($key) use ($entry_moments) {
                    return $entry_moments[$key];
                },
                $entry_moment_keys));
            break;

        case 'type':
            echo TYPE_OPTIONS[carbon_get_post_meta($post_id, 'question_type')];
            break;
    }
}

add_action('rest_api_init', function () {
    register_rest_route('wedding-RSVP/v1', '/question/via-entry-moment', array(
        'methods' => 'GET',
        'callback' => 'retrieve_questions_via_entry_moment',
        'permission_callback' => '__return_true',
        'args' => array(
            'entry_moment' => array(
                'required' => true,
            ),
        ),
    ));
});

function retrieve_questions_via_entry_moment($data)
{
    $query = new WP_Query(array(
        'post_type' => 'w_rsvp_question',
        'post_status' => 'publish',
        'posts_per_page' => -1
    ));

    $question_ids = array_map(function ($question) {
        return $question->ID;
    }, $query->get_posts());
    $filtered_ids = array_filter($question_ids, function ($id) use ($data) {
        return in_array($data['entry_moment'], carbon_get_post_meta($id, 'question_show_for_entry_moments'));
    });

    $response_body = array();
    foreach ($filtered_ids as $id) {
        $response_body[] = array(
            'key' => carbon_get_post_meta($id, 'question_key'),
            'position' => carbon_get_post_meta($id, 'question_position'),
            'show_per_guest' => carbon_get_post_meta($id, 'question_show_per_guest'),
            'labels' => carbon_get_post_meta($id, 'question_labels'),
            'type' => carbon_get_post_meta($id, 'question_type'),
            'possible_values' => carbon_get_post_meta($id, 'possible_values'),
            'hide_for_questions' => carbon_get_post_meta($id, 'question_hide_for_question_value'),
        );
    }

    return new WP_REST_Response(
        array(
            'status' => 200,
            'body_response' => json_encode($response_body)
        )
    );
}