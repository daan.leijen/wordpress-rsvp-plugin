<form id="rsvp_form_passcode">
    <label>Passcode</label><br/>
    <input type="text" id="passcode" oninput="this.value = this.value.toUpperCase()"><br/><br/>
    <button type="submit"><?php echo str_replace(["\r\n", "\r", "\n"], "<br/>", get_plugin_options('w_rsvp_passcode_retrieve_button'));?></button>
</form>

<form id="rsvp_form_questions" hidden = true>
    <h4 id="greeting"><?php echo str_replace(["\r\n", "\r", "\n"], "<br/>", get_plugin_options('w_rsvp_greeting_message'));?></h4>
    <div id="rsvp_questions"></div>
    <button type="submit"><?php echo str_replace(["\r\n", "\r", "\n"], "<br/>", get_plugin_options('w_rsvp_submit_button'));?></button>
</form>

<div id="submitted_message" hidden="true"><?php echo str_replace(["\r\n", "\r", "\n"], "<br/>", get_plugin_options('w_rsvp_submit_message'));?></div>
<div id="already_submitted_message" hidden="true"><?php echo str_replace(["\r\n", "\r", "\n"], "<br/>", get_plugin_options('w_rsvp_already_submitted_message'));?></div>

<script>
    jQuery(document).ready(function ($) {
        let guest_group;
        let questions;

        const determine_visibility_func = function () {
            let questions_to_hide = [];
            let questions_to_hide_per_guest = {};

            $("#rsvp_questions").children("input:radio").not('visible').attr("required", "required");
            $("#rsvp_questions").children().not('visible').show();

            questions.forEach(function (question) {
                if (!question['show_per_guest']) {
                    question['hide_for_questions'].forEach(function (condition) {
                        if (!questions_to_hide.includes(question)) {
                            let radio_val = $("input:radio[name = " + condition['key'] + "]:checked").val();
                            if (radio_val === condition['value_key']) {
                                questions_to_hide.push(question);
                            }
                        }
                    });
                } else {
                    guest_group['guests'].forEach(function (guest) {
                        question['hide_for_questions'].forEach(function (condition) {
                            if (!questions_to_hide.includes(question)) {
                                let radio_val = $("input:radio[name = " + condition['key'] + "_" + guest.replace(/\s+/g, '-') + "]:checked").val();
                                if (radio_val === condition['value_key']) {
                                    if (questions_to_hide_per_guest.hasOwnProperty(question['key'])) {
                                        questions_to_hide_per_guest[question['key']].push(guest);
                                    } else {
                                        questions_to_hide_per_guest[question['key']] = [];
                                        questions_to_hide_per_guest[question['key']].push(guest);
                                    }
                                }
                            }
                        });
                    });
                }
            });

            questions_to_hide.forEach(question => {
                $("div[name=" + question['key'] + "]",).hide();
            });

            for (const question in questions_to_hide_per_guest) {
                if (questions_to_hide_per_guest[question].length === guest_group['guests'].length) {
                    $("div[name=" + question + "]").hide();
                } else {
                    questions_to_hide_per_guest[question].forEach(guest => {
                        $("div[name=" + question + "_" + guest.replace(/\s+/g, '-') + "]").hide();
                        $("tr[class=" + question + "_" + guest.replace(/\s+/g, '-') + "]").hide();
                    })
                }
            }

            $(":hidden :radio").removeAttr("required");
        };

        $("#rsvp_questions").on('change', "input[type=radio]", determine_visibility_func);

        function change_to_RSVP_form() {
            $("#rsvp_form_questions").show();
            $("#greeting").html(function (index, value) {
                return value.replaceAll("\{guests\}", guest_group['guests_joined'])
            });

            retrieve_and_load_questions().then(() => determine_visibility_func());
        }

        function retrieve_and_load_questions() {
            const promise = new Promise((resolve, reject) => $.ajax({
                type: "GET",
                url: "<?php echo get_rest_url(null, 'wedding-RSVP/v1/question/via-entry-moment');?>",
                data: {
                    entry_moment: guest_group['entry-moment-raw']
                },
                success: function (res) {
                    resolve(res);
                },
                error: function (err) {
                    reject(err);
                    alert("An error occurred. Please try again later.");
                }
            }));

            promise.then((data) => {
                questions = jQuery.parseJSON(data['body_response']);
                questions.sort(function (questionA, questionB) { return questionA['position'] - questionB['position']})
                return new Promise(() => load_questions());
            })

            return promise;
        }

        function load_questions() {
            let question_html;
            questions.forEach(function (question) {
                if (question['type'] === 'radio') {
                    question_html = generate_question_html_radio(question);
                } else {
                    question_html = generate_question_html_textarea(question);
                }

                $("#rsvp_questions").html(function (index, value) {
                    let newVal = value;
                    if (newVal.length > 0) {
                        newVal += '<br/>';
                    }

                    newVal += question_html;
                    return newVal;
                });
            })
        }

        function generate_question_html_radio(question) {
            let html = "<div style=\"overflow-x:auto;\" name=\"" + question['key'] + "\"><table><tr>";
            question['labels'].forEach(function (label) {
                html += "<th><b>" + label['label'] + "</b></th>";
            })
            html += "</tr></table>";

            html += "<table>";
            if (question['show_per_guest']) {
                guest_group['guests'].forEach(function (guest, guest_index) {
                    question['possible_values'].forEach(function (possible_value, value_index) {
                        html += "<tr class=\"" + question['key'] + "_" + guest.replace(/\s+/g, '-') + "\">";
                        if (value_index === 0) {
                            html += "<td rowspan=\"" + question['possible_values'].length + "\"" +
                                ((guest_index !== 0) ? " style=\"border-top: thin solid;\"" : "") + "><label>"
                                + guest + "</label></td>"
                        }
                        html += "<td" + ((value_index === 0 && guest_index !== 0) ? " style=\"border-top: thin solid;\"" : "")
                            + "><input type=\"radio\" name=\"" + question['key'] + "_" + guest.replace(/\s+/g, '-') + "\"  value=\"" + possible_value['key'] + "\"  id=\"" + possible_value['key'] + "\" required=\"required\"></td>";
                        html += "<td" + ((value_index === 0 && guest_index !== 0) ? " style=\"border-top: thin solid;\"" : "")
                            + "><label for=\"" + possible_value['key'] + "\">"
                        possible_value['labels'].forEach(function (answer_label) {
                            html += answer_label['label'] + "<br/>";
                        });
                        html += "</label></td></tr>";
                    });
                });
            } else {
                question['possible_values'].forEach(function (possible_value) {
                    html += "<tr><td><input type=\"radio\" name=\"" + question['key'] + "\"  value=\"" + possible_value['key'] + "\"  id=\"" + possible_value['key'] + "\" required=\"required\"></td>";
                    html += "<td><label for=\"" + possible_value['key'] + "\">"
                    possible_value['labels'].forEach(function (answer_label) {
                        html += answer_label['label'] + "<br/>";
                    })
                    html += "</label></td></tr>";
                })
            }
            html += "</table>";

            html += "</div>";

            return html;
        }

        function generate_question_html_textarea(question) {
            let html = "<div style=\"overflow-x:auto;\" name=\"" + question['key'] + "\"><table><tr>";
            question['labels'].forEach(function (label) {
                html += "<th><b>" + label['label'] + "</b></th>";
            })
            html += "</tr></table>";

            if (question['show_per_guest']) {
                guest_group['guests'].forEach(function (guest) {
                    html += "<div name=\"" + question['key'] + "_" + guest.replace(/\s+/g, '-') + "\">";
                    html += "<label>" + guest + "</label><br/>";
                    html += "<textarea rows=\"4\" cols=\"50\" name=\"" + question['key'] + "_" + guest.replace(/\s+/g, '-') + "\"  id=\"" + question['key'] + "\"></textarea>";
                    html += "</div>";
                });
            } else {
                html += "<textarea rows=\"4\" cols=\"50\" name=\"" + question['key'] + "\"  id=\"" + question['key'] + "\"></textarea>";
            }

            html += "</div>";

            return html;
        }

        $("#rsvp_form_passcode").submit(function (event) {

            event.preventDefault();

            $.ajax({
                type: "GET",
                url: "<?php echo get_rest_url(null, 'wedding-RSVP/v1/guest-group');?>",
                data: {
                    passcode: $("#passcode").val()
                },
                success: function (res) {
                    $("#rsvp_form_passcode").hide();
                    guest_group = jQuery.parseJSON(res['body_response']);
                    if (guest_group['has_responded']) {
                        $("#already_submitted_message").show();
                    } else {
                        change_to_RSVP_form();
                    }
                },
                error: function () {
                    alert("<?php echo str_replace(["\r\n", "\r", "\n"], "\\n", get_plugin_options('w_rsvp_wrong_passcode_message'));?>");
                }
            })
        });

        $("#rsvp_form_questions").submit( function(event){

            event.preventDefault();

            let guest_answers = {};
            guest_group['guests'].forEach(function (guest) {

                let choice_answers = {};
                let comment_answers = {};
                questions.forEach(function (question) {
                    if (question['show_per_guest']) {
                        if (question['type'] === 'radio') {
                            const question_in_form = $("input:radio[name = " + question['key'] + "_" + guest.replace(/\s+/g, '-') + "]:checked");
                            choice_answers[question['key']] = question_in_form.val();
                        } else {
                            comment_answers[question['key']] = $("textarea[name = " + question['key'] + "_" + guest.replace(/\s+/g, '-') + "]").val();
                        }
                    } else {
                        if (question['type'] === 'radio') {
                            const question_in_form = $("input:radio[name = " + question['key'] + "]:checked");
                            choice_answers[question['key']] = question_in_form.val();
                        } else {
                            comment_answers[question['key']] = $("textarea[name = " + question['key'] + "]").val();
                        }
                    }
                });

                guest_answers[guest] = {
                    choice_answers : choice_answers,
                    comment_answers : comment_answers
                };
            });

            $.ajax({
                type:"POST",
                url: "<?php echo get_rest_url(null, 'wedding-RSVP/v1/rsvp-response/submit');?>",
                data: {
                    form: JSON.stringify({
                        'guests': guest_group,
                        'guest_answers': guest_answers
                    })
                },
                success:function(res){
                    $("#rsvp_form_questions").hide();
                    $("#submitted_message").show();
                },
                error: function(){

                    $("#form_error").html("There was an error submitting").fadeIn();
                }


            })


        });
    });

</script>