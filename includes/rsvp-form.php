<?php

if (!defined('ABSPATH')) {
      die('You cannot be here');
}

add_shortcode('wedding-rsvp', 'show_rsvp_form');

add_action('wp_enqueue_scripts', 'enqueue_custom_scripts');

function enqueue_custom_scripts()
{
}

function show_rsvp_form()
{
      include MY_PLUGIN_PATH . '/includes/templates/rsvp-form.php';
}