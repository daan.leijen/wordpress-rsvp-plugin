<?php

if (!defined('ABSPATH')) {
    die('You cannot be here');
}

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action('after_setup_theme', 'load_carbon_fields');
add_action('carbon_fields_register_fields', 'create_options_page');

function load_carbon_fields()
{
    \Carbon_Fields\Carbon_Fields::boot();
}

function create_options_page()
{

    Container::make('theme_options', 'Wedding RSVP Options')
        ->set_page_menu_position(300)
        ->set_icon('dashicons-media-text')
        ->add_fields(array(

            Field::make('text', 'w_rsvp_passcode_format', 'Passcode format')
                ->set_default_value('****##')
                ->set_help_text('Use * for characters, # for digits and ! for special characters.'),

            Field::make('text', 'w_rsvp_entry_moments', 'Entry moments')
                ->set_default_value('Ceremony,Dinner,Evening')
                ->set_help_text('Provide a comma separated list of all entry moments for guests.'),

            Field::make('textarea', 'w_rsvp_passcode_retrieve_button', 'Passcode retrieval label')
                ->set_help_text('Type the message you want as the label for retrieving the details based on the passcode.'),

            Field::make('textarea', 'w_rsvp_wrong_passcode_message', 'Wrong passcode message')
                ->set_help_text('Type the message you want to show if the passcode is incorrect.'),

            Field::make('textarea', 'w_rsvp_greeting_message', 'Greeting')
                ->set_help_text('Type the message you want to show as a greeting on top of the question form. 
                                Use {guests} in the text to have it replaced with the names of the guests belonging 
                                to the entered passcode.'),

            Field::make('textarea', 'w_rsvp_submit_button', 'RSVP submit label')
                ->set_help_text('Type the message you want as the label for submitting the RSVP form.'),

            Field::make('textarea', 'w_rsvp_submit_message', 'Confirmation Message')
                ->set_help_text('Type the message you want the submitter to receive.'),

            Field::make('textarea', 'w_rsvp_already_submitted_message', 'Already submitted message')
                ->set_help_text('Type the message you want to show if a passcode is entered again after submitting.'),

        ));
}
