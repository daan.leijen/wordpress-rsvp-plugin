<?php
/**
 *
 * Plugin name: Wedding RSVP
 * Description: Plugin for wedding RSVP forms and registration
 * Author: Daan Leijen-Lam
 * Version: 1.0.6
 * Text Domain: wedding-RSVP
 *
 */

if (!defined('ABSPATH')) {
    die('You cannot be here');
}

if (!class_exists('WeddingRSVPPlugin')) {


    class WeddingRSVPPlugin
    {


        public function __construct()
        {

            define('MY_PLUGIN_PATH', plugin_dir_path(__FILE__));

            define('MY_PLUGIN_URL', plugin_dir_url(__FILE__));

            require_once(MY_PLUGIN_PATH . '/vendor/autoload.php');

        }

        public function initialize()
        {
            include_once MY_PLUGIN_PATH . 'includes/utilities.php';

            include_once MY_PLUGIN_PATH . 'includes/options-page.php';

            include_once MY_PLUGIN_PATH . 'includes/guest-group.php';

            include_once MY_PLUGIN_PATH . 'includes/question.php';

            include_once MY_PLUGIN_PATH . 'includes/rsvp-response.php';

            include_once MY_PLUGIN_PATH . 'includes/rsvp-form.php';
        }


    }

    $weddingRSVPPlugin = new WeddingRSVPPlugin;
    $weddingRSVPPlugin->initialize();

}